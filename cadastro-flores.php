<!DOCTYPE html>
<html lang="pt">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cadstro de Flores</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/cadastro-flores.css">
    <link rel="stylesheet" href="css/reset.css">
</head>
<body>
    <main id="interface">
        <header class="espacamento">
            <h1>Cadastre Flores</h1><!--PEsquisar a curva rosa se da pra fazer com background do h1 ou se tem que colocar uma div envolta -->
        </header>
        
        <form action="" method="POST">
        <h2 class="espacamento">Cadastre as flores de acordo com o mês que elas floresce</h2>
            <section>
                <div class="nomeEspecie">
                    <div class="espacamento2">
                        <label for="nome">Nome</label><br>
                        <input type="text" name="nome" id="nome" placeholder="Qual o nome da flor">
                        
                    </div>
                    <div class="espacamento2">
                        <label for="especie">Espécie </label><br>
                        <input type="text" name="especie" id="especie" placeholder="Qual o nome da espécie">
                        
                    </div>
                </div>
            
                <div class="fileContainer">
                    <label for="image">
                        <img src="img/vector.png" alt="Imagem Selecionada">
                        <input type="file" name="image" id="foto">
                    </label>
                    <p>Escolha uma imagem</p>
                </div>
            </section>

            <div class="espacamento2">
                <label for="desc">Descrição</label><br>
                <input type="text" name="desc" id="desc" placeholder="Escreva uma breve descrição sobre a flor">
            </div>

            <div class="espacamento2">
                <p>Quais meses a flor irá florescer</p>
                <input type="button" class="button radio" id="button1" value="Jan" onClick="toggleButton(this);">
                <input type="button" class="button radio" id="button1" value="Fev" onClick="toggleButton(this);">
                <input type="button" class="button radio" id="button1" value="Mar" onClick="toggleButton(this);">
                <input type="button" class="button radio" id="button1" value="Abril" onClick="toggleButton(this);">
                <input type="button" class="button radio" id="button1" value="Maio" onClick="toggleButton(this);">
                <input type="button" class="button radio" id="button1" value="Jun" onClick="toggleButton(this);">
                <input type="button" class="button radio" id="button1" value="Jul" onClick="toggleButton(this);">
                <input type="button" class="button radio" id="button1" value="Ago" onClick="toggleButton(this);">
                <input type="button" class="button radio" id="button1" value="Set" onClick="toggleButton(this);">
                <input type="button" class="button radio" id="button1" value="Out" onClick="toggleButton(this);">
                <input type="button" class="button radio" id="button1" value="Nov" onClick="toggleButton(this);">
                <input type="button" class="button radio" id="button1" value="Dez" onClick="toggleButton(this);">
            </div>

            <div class="espacamento">
                <label for="abelha">Selecione as abelhas que polinizam essas flores</label><br>
                    <select id="abelha">
                        <option value=""></option>
                        <option value="urucu">Uruçu (Melipona scutellaris);</option>
                        <option value="urucu-amarela">Uruçu-Amarela (Melipona rufiventris);</option>
                        <option value="guarupu">Guarupu (Melipona bicolor);</option>
                        <option value="irai">Iraí (Nannotrigona testaceicornes);</option>
                    </select>
            </div>
            <div class="controle">
                <button type="reset">Cancelar</button>
                <button type="submit">Cadastrar flor</button>
            </div>
        </form>

        <div class="imagens">
            <img src="img/flor11.png" alt="">
            <img src="img/flor34.png" alt="">
        </div>  
    </main>
</body>
</html>