<!DOCTYPE html>
<html lang="pt">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Home</title>
    <link rel="stylesheet" href="bootstrap-4/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <main class="container">
        <div class="row">
            <div class="col-md-12 bg-light">
                <form action="" method="post">
                    <div class="row mt-5">
                        <div class="col-md-5 offset-md-2">
                            <h1>Calendário de Flores</h1>
                        </div>
                        <div class="col-md-3 align-self-center">
                            <a href="" target="_blank"><button>Cadastrar flor</button></a>
                        </div>        
                    </div>    
                        <div class="row">
                            <div class="col-md-3 offset-md-7">
                                <a href="" target="_blank"><button>Cadastrar abelha</button></a>
                            </div>    
                        </div>
                    
                    <div class="row">
                        <div class="col-md-4 offset-md-2">
                            <p>
                                Neste calendário encontram-se diversas flores.
                                Podem ser agupada pelos meses que florescem e o
                                pelo tipo de abelha que poliniza a flor.
                            </p>  
                        </div>      
                    </div>
    
                    <div class="row">
                        <div class="form-group col-md-7 offset-md-2">
                            <label for="abelha">Selecione as abelhas</label>
                            <select id="abelha" class="form-control">
                                <option value=""></option>
                                <option value="urucu">Uruçu (Melipona scutellaris);</option>
                                <option value="urucu-amarela">Uruçu-Amarela (Melipona rufiventris);</option>
                                <option value="guarupu">Guarupu (Melipona bicolor);</option>
                                <option value="irai">Iraí (Nannotrigona testaceicornes);</option>
                            </select>
                        </div>
                    </div>
    
                    <div class="row">
                        <div class="col-md-7 offset-md-2">
                            <div class="form-group">
                                <label>Escolha os meses</label>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row justify-content-between">
                                            <div class="col-md-1 align-self-center">
                                                <label for="Jan">
                                                    <input type="button" class="form-check-input btnMes" id="Jan" value="Jan" name="" onClick="toggleButton(this);">
                                                </label>
                                            </div>
                                            <div class="col-md-1">
                    
                                                <label for="Fev"><input type="button" class="form-check-input btnMes" id="Fev" value="Fev" name="Fev" onClick="toggleButton(this);"></label>
                                            </div>
                                            <div class="col-md-1">
                                                <label for="Mar"><input type="button" class="form-check-input btnMes" id="Mar" value="Mar" name="Mar" onClick="toggleButton(this);"></label>
                                            </div>
                                            <div class="col-md">
                                                <label for="Abril"><input type="button" class="form-check-input btnMes" id="Abril" value="Abril" name="Abril" onClick="toggleButton(this);"></label>
                                            </div>
                                            <div class="col-md">
                                                <label for="Maio"><input type="button" class="form-check-input btnMes" id="Maio" value="Maio" name="Maio" onClick="toggleButton(this);"></label>
                                            </div>
                                            <div class="col-md">
                                                <label for="Jun"><input type="button" class="form-check-input btnMes" id="Jun" value="Jun" name="Jun" onClick="toggleButton(this);"></label>
                                            </div>
                                            <div class="col-md">
                                                <label for="Jul"><input type="button" class="form-check-input btnMes" id="Jul" value="Jul" name="Jul" onClick="toggleButton(this);"></label>
                                            </div>
                                            <div class="col-md">
                                                <input type="button" class="form-check-input btnMes" id="Ago" value="Ago" name="Ago" onClick="toggleButton(this);">
                                            </div>
                                            <div class="col-md">
                                                <input type="button" class="form-check-input btnMes" id="Set" value="Set" name="Set" onClick="toggleButton(this);">
                                            </div>
                                            <div class="col-md">
                                                <input type="button" class="form-check-input btnMes" id="Out" value="Out" name="Out" onClick="toggleButton(this);">
                                            </div>
                                            <div class="col-md">
                                                <input type="button" class="form-check-input btnMes" id="Nov" value="Nov" name="Nov" onClick="toggleButton(this);">
                                            </div>
                                            <div class="col-md">
                                                <input type="button" class="form-check-input btnMes" id="Dez" value="Dez" name="Dez" onClick="toggleButton(this);">
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>
    
                </form>    
    
                <section>

                    <div class="row mt-5">
                        <div class="col-md-8 offset-2">
                            <div class="row">
                                <div class="col-md-2 bg-info">
                                    <img src="" alt="">
                                    <figcaption>Azaleia Rosa</figcaption>
                                </div>
                                <div class="col-md-2 bg-light">
                                    <img src="" alt="">
                                    <figcaption>Laranjeira</figcaption>
                                </div>
                                <div class="col-md-2 bg-light">
                                    <img src="" alt="">
                                    <figcaption>Mangueira</figcaption>
                                </div>
                                <div class="col-md-2 bg-light">
                                    <img src="" alt="">
                                    <figcaption>Manjericão</figcaption>
                                </div>
                            </div>
               
            
                            <div class="row">
                                <div class="col-md-2 bg-light">
                                    <figcaption>Oregano</figcaption>
                                </div>
                                <div class="col-md-2 bg-light">
                                    <figcaption>Quaresmeira</figcaption>
                                </div>
                                <div class="col-md-2 bg-light">
                                    <figcaption>Pessegueiro</figcaption>
                                </div>
                                <div class="col-md-2 bg-light">
                                    <figcaption>Pitanga</figcaption>
                                </div>
                            </div>
            
                            <div class="row">
                                <div class="col-md-2 bg-light">
                                    <figcaption>Salvia</figcaption>
                                </div>
                                <div class="col-md-2 bg-light">
                                    <figcaption>Sabugueiro</figcaption>
                                </div>
                                <div class="col-md-2 bg-light">
                                    <figcaption>Sibipuruna</figcaption>
                                </div>
                                <div class="col-md-2 bg-light">
                                    <figcaption>Tipuana</figcaption>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
    
    
                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-center">
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                                <span class="sr-only">Previous</span>
                            </a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                                <span class="sr-only">Next</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </main>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="bootstrap-4/js/bootstrap.min.js"></script>
</body>
</html>