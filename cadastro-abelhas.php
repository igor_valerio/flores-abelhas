<!DOCTYPE html>
<html lang="pt">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cadastro de abelhas</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/cadastro-abelhas.css">
    <link rel="stylesheet" href="css/reset.css">
</head>
<body>
    <main id="interface">
        <header class="espacamento">
            <h1>Cadastre abelhas</h1>
        </header>
        <form action="" method="post">
            <h2 class="espacamento">Cadastre as abelhas</h2>
            <div class="espacamento2">
                <label for="nome">Nome</label><br>
                <input type="text" name="nome" id="nome" placeholder="Qual o nome da abelha">
            </div>
            <div>
                <label for="especie">Espécie</label><br>
                <input type="text" name="especie" id="especie" placeholder="Qual a espécie ou nome ciêntífico">
            </div>
            <div class="controle">
                <button type="reset">Cancelar</button>
                <button type="submit">Cadastrar abelha</button>
            </div>
        </form>
        <div class="imagens">
            <img src="img/flor11.png" alt="">
            <img src="img/flor34.png" alt="">
        </div> 
    </main>
</body>
</html>